# CI/CD

gitlab ciを利用して、CIやCDを試すリポジトリ。

## 関連記事
* [GitLab CI/CD を利用した継続的デプロイの試行錯誤](https://scrapbox.io/tsuchinaga/GitLab_CI%2FCD_%E3%82%92%E5%88%A9%E7%94%A8%E3%81%97%E3%81%9F%E7%B6%99%E7%B6%9A%E7%9A%84%E3%83%87%E3%83%97%E3%83%AD%E3%82%A4%E3%81%AE%E8%A9%A6%E8%A1%8C%E9%8C%AF%E8%AA%A4)

## Dockerfile
Dockerfileのbuild時には下記を設定する必要があります

* GITLAB_RUNNER_URL: url ex) https://gitlab.com/
* ARG GITLAB_RUNNER_TOKEN: runnerのtoken ex) uK9pSW6r5U5qErRxfZxz
* ARG GITLAB_RUNNER_DESCRIPTION: runnerの説明 ex) ci/cd test

以上を含めるとこんな感じでbuildすることでregisterもされます。

`$ docker build -t tsuchinaga/runner:0.1 --build-arg GITLAB_RUNNER_URL="https://gitlab.com/" --build-arg GITLAB_RUNNER_TOKEN="uK9pSW6r5U5qErRxfZxz" --build-arg GITLAB_RUNNER_DESCRIPTION="ci/cd test" .`
