FROM debian:buster-slim

# 環境変数
ENV GITLAB_RUNNER_DISABLE_SKEL=true

# 必須コマンドの準備
RUN apt update -y && \
    apt install -y curl

# gitlab-runnerのinstall
RUN curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash && \
    apt install -y gitlab-runner

# 引数
ARG GITLAB_RUNNER_URL
ARG GITLAB_RUNNER_TOKEN
ARG GITLAB_RUNNER_DESCRIPTION

RUN gitlab-runner register \
      --non-interactive \
      --url "$GITLAB_RUNNER_URL" \
      --registration-token "$GITLAB_RUNNER_TOKEN" \
      --description "$GITLAB_RUNNER_DESCRIPTION" \
      --executor "shell" \
      --tag-list "deploy"

CMD gitlab-runner run --working-directory /home/gitlab-runner --config /etc/gitlab-runner/config.toml --service gitlab-runner --syslog
