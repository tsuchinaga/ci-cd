package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
)

func main() {
	fmt.Println("こんにちわーるど")

	ln, _ := net.Listen("tcp", ":80")
	if err := http.Serve(ln, http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		_, _ = w.Write([]byte("200 OK"))
	})); err != nil {
		log.Fatalln(err)
	}
}
